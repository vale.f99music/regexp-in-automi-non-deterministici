Componenti del gruppo:

Iris Ermini 845310
Valeria Froio 847272

Commenti e descrizioni del programma svolto:

Predicati principali:
1.	(is-regexp RE)
2.	(nfa-regexp-comp RE)
3.	(nfa-test FA Input)

Predicati d�appoggio utilizzati:
-	(is-compound RE) restituisce T se RE � una Sexp con primo elemento non riservato.
-	(is-regularexp RE) viene utilizzata per la ricorsione della (is-regexp RE) sugli argomenti di seq e or. 
-	(is-reserved RE) restituisce T se RE � una Sexp il cui primo atomo � riservato.
-	(nfa-regexp-comp-switch RE &optional A B) chiama le funzioni specifiche per la creazione dell�automa a seconda dei vari casi.
-	(nfa-regexp-comp-notres RE &optional A B), (nfa-regexp-comp-star RE &optional A B), (nfa-regexp-comp-plus RE &optional A B), (nfa-regexp-comp-seq RE &optional A B) e (nfa-regexp-comp-or RE &optional A B) sono le funzioni chiamate da (nfa-regexp-comp-switch RE &optional A B).
-	(nfa-regexp-comp-seq-ric RE X F) viene utilizzata per la ricorsione sugli argomenti di seq.
-	(nfa-regexp-comp-or-ric RE X F)  viene utilizzata per la ricorsione sugli argomenti di or.
-	(nfa-regexp-comp-seq-ric-res RE X F) e (nfa-regexp-comp-or-ric-res RE X F) funzionano in modo analogo a (nfa-regexp-comp-seq-ric RE X F) e (nfa-regexp-comp-or-ric RE X F), ma gestiscono un argomento riservato.
-	(prof1 l) restituisce la lista �l� passata in ingresso con profondit� 1. 
-	(length-l l) restituisce la lunghezza della lista �l� passata in input.
-	(last-l l) restituisce l�ultimo elemento della lista �l�.
-	(is-FA FA) controlla che �FA� abbia la corretta struttura di un automa.
-	(state-list FA state) restituisce la lista contenente tutte le mosse che si possono fare in un automa FA a partire da uno stato �state�.
-	(nfa-test-ric FA Input State-moves) restituisce T se, consumata tutta la lista �Input�, ci si trova nello stato finale di FA.
-	(is-final FA state) restituisce T se lo stato �state� � uno stato finale dell'automa FA.
-	(is-initial FA state) restituisce T se lo stato �state� � uno stato iniziale dell'automa FA.
-	(starting-point FA FA-static) restituisce l�automa FA se � presente uno stato iniziale.

1) (is-regexp RE)
-	utilizza (is-compound RE) per distinguere le Sexp con primo elemento non riservato da tutto ci� che non deve essere accettato.
-	controlla che non si possano avere plus, star, seq e or come atomi.
-	plus e star possono avere solo un argomento.
-	viene controllato che vi siano almeno due argomenti per il caso or.
-	richiama (is-regularexp RE) nel caso in cui abbiamo seq oppure or per applicare (is-regexp RE) a ogni argomento di seq oppure di or.

2) (nfa-regexp-comp RE) :
-	controlla che l�espressione passata sia regolare con (is_regexp RE).
-	chiama (nfa-regexp-comp-switch RE &optional A B).
-	(nfa-regexp-comp-switch RE &optional A B) e le funzioni specifiche hanno gli argomenti opzionali A e B che fungono da stati iniziali e finali in caso di strutture annidate nell�automa.
-	(nfa-regexp-comp-switch RE &optional A B) utilizza (prof1 l) per restituire l�automa con profondit� 1.
-	le funzioni specifiche utilizzano (gensym �p�) per generare nomi per gli stati.
-	(nfa-regexp-comp-notres RE &optional A B) crea l�automa per atomi e Sexp con primo elemento non riservato (not reserved).
-	in ogni funzione specifica vengono distinti due casi: quando ci si trova in una struttura annidata (A e B non sono nil) e quando non c�� una struttura annidata.
-	le funzioni specifiche chiamano (is_reserved RE) per stabilire se un argomento ha funtore riservato oppure non riservato e trattano i due casi diversamente.
-	se hanno un argomento riservato, le funzioni specifiche di star e plus chiamano (nfa-regexp-comp-switch RE &optional A B).
-	se hanno un argomento riservato, le funzioni specifiche di seq e or chiamano rispettivamente (nfa-regexp-comp-seq-ric-res RE X F) e (nfa-regexp-comp-or-ric-res RE X F), in caso contrario chiamano (nfa-regexp-comp-seq-ric RE X F) e (nfa-regexp-comp-or-ric RE X F).
-	(nfa-regexp-comp-seq-ric-res RE X F) e (nfa-regexp-comp-seq-ric RE X F) sono mutuamente ricorsive, cos� come (nfa-regexp-comp-or-ric-res RE X F)  e (nfa-regexp-comp-or-ric RE X F).

3) (nfa-test FA Input):
-	chiama (is-FA FA) se FA � una lista.
-	(is-FA FA) controlla che gli elementi di FA siano liste contenenti tre elementi, con l�utilizzo di (length-l l).
-	stampa un messaggio di errore se FA non ha la struttura di un automa.
-	utilizza (state-list FA state) e ne salva localmente il valore in State-moves per passarlo a (nfa-test-ric FA Input State-moves).
-	(nfa-test-ric FA Input State-moves) utilizza (is-final FA state) per accettare l�Input una volta nil.
-	(nfa-test-ric FA Input State-moves) controlla se State-moves contiene una sola mossa o due: nel caso in cui ci siano due mosse viene richiamata ricorsivamente due volte passandole una volta la prima mossa e l�altra volta la seconda e queste chiamate vengono inserite come argomenti di una funzione or per fare in modo che accetti se una delle due strade restituisce T.
-	Nel caso in cui c�� solo una mossa, (nfa-test-ric FA Input State-moves) fa una chiamata ricorsiva e non consuma l�Input, se � una epsilon-mossa; in caso contrario controlla che il primo elemento dell�Input sia uguale al �simbolo� della mossa contenuta in State-moves e se � cos�, consuma l�Input e richiama ricorsivamente la funzione, altrimenti chiama ricorsivamente la funzione senza consumare l�Input e chiama (state-list FA state) passandole il risultato di (starting-point FA FA-static).
-	la funzione (starting-point FA FA-static) � stata creata per far s� che vengano prese in esame tutte le possibilit� dell�automa di or prima di confermare che l�Input non sia accettato dall�automa.
-	(starting-point FA FA-static) utilizza (is-initial FA state).
-	(is-final FA state) confronta lo stato �state� con lo stato finale che viene ricavato considerando il risultato di (last-l l). 
