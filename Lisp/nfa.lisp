;; Iris Ermini 845310
;; Valeria Froio 847272


(defun is-compound (RE)
    (and (listp RE) 
       	(not (or (equal (car RE) 'star)
                (equal (car RE) 'plus)
                (equal (car RE) 'seq)
                (equal (car RE) 'or)
            )
        )
    )
)


;; is-regexp

(defun is-regexp (RE)
    (if (and (listp RE) (null (second RE))) nil 
        (cond ((and (atom RE)
                (not (equal RE 'star))
                (not (equal RE 'plus))
                (not (equal RE 'seq))
                (not (equal RE 'or)) T))
            ((and (equal 'star (car RE))
                (null (third RE))
                (is-regexp(second RE))) T)  
            ((and (equal 'plus (car RE)) 
                (null (third RE))
                (is-regexp(second RE))) T)
            ((and (equal 'seq (car RE))
                (is-regularexp (cdr RE))) T)
            ((and (equal 'or (car RE))
                (not (null (third RE)))
                (is-regularexp (cdr RE))) T)
            ((is-compound RE) T)
            (T nil)
        )
    )
)



(defun is-regularexp (RE)
    (if (null RE) 
        T
        (and (is-regexp (car RE))
           (is-regularexp (cdr RE))
        )
    )
)

    
(defun is-reserved (RE)
    (if (listp (car RE))
		(is-reserved (car RE))
		(if (and (listp RE)
				(or (equal (car RE) 'star)
					(equal (car RE) 'plus)
					(equal (car RE) 'seq)
					(equal (car RE) 'or)
                )
            )
			T
			nil
        )
    )
)


(defun prof1 (l)
    (cond ((null l) nil)
        ((and (equal 3 (length-l l))
            (atom (first l))
            (atom (third l))) 
            (list l))
        (T (append (prof1 (car l))
                (prof1 (cdr l))
            )
        )
    )
)


;; nfa-regexp-comp

(defun nfa-regexp-comp (RE)
    (if (is-regexp RE)
        (nfa-regexp-comp-switch RE)
        nil
    )
)


(defun nfa-regexp-comp-switch (RE &optional A B)
    (if (is-regexp RE)
		(cond ((or (atom RE) (is-compound RE)) 
		       (nfa-regexp-comp-notres RE A B))
			((equal (car RE) 'star) 
			 (prof1 (nfa-regexp-comp-star RE A B)))
			((equal (car RE) 'plus) 
			 (prof1 (nfa-regexp-comp-plus RE A B)))
			((equal (car RE) 'seq) 
			 (prof1 (nfa-regexp-comp-seq RE A B)))
			((equal (car RE) 'or) 
			 (prof1 (nfa-regexp-comp-or RE A B)))
            (T nil)
		)
		nil
	)
)


;; Casi base: Sexp con primo elemento non riservato e atomic

(defun nfa-regexp-comp-notres (RE &optional A B)
	(let ((X (gensym "p")) (Y (gensym "p"))) 
        (if (and (null A) (null B))
            (let ((S (gensym "p")) (F (gensym "p")))
				(list (list S 'epsilon X)
					(list X RE Y)
					(list Y 'epsilon F)
				)
			)
			(list (list A 'epsilon X)
				(list X RE Y)
				(list Y 'epsilon B)
			)
		)
	)
)


;; star

(defun nfa-regexp-comp-star (RE &optional A B)
    (let ((X (gensym "p"))(Y (gensym "p")))
		(if (and (null A) (null B))
			(let ((S (gensym "p"))(F (gensym "p")))
				(if (is-reserved (cdr RE))
					(list (list S 'epsilon X)
						(nfa-regexp-comp-switch (second RE) X Y)
						(list X 'epsilon F)
						(nfa-regexp-comp-switch (second RE) Y X)
						(list Y 'epsilon F)
					)
					(list (list S 'epsilon X)
						(list X (second RE) Y)
						(list X 'epsilon F)
						(list Y (second RE) X)
						(list Y 'epsilon F)
					)
				)
			)
			(if (is-reserved (cdr RE))
				(list (list A 'epsilon X)
					(nfa-regexp-comp-switch (second RE) X Y)
					(list X 'epsilon B)
					(nfa-regexp-comp-switch (second RE) Y X)
					(list Y 'epsilon B)
				)
				(list (list A 'epsilon X)
					(list X (second RE) Y)
					(list X 'epsilon B)
					(list Y (second RE) X)
					(list Y 'epsilon B)
				)
			)
		)
	)
)


;; plus

(defun nfa-regexp-comp-plus (RE &optional A B)
    (let ((X (gensym "p"))(Y (gensym "p"))
        (W (gensym "p"))(Z (gensym "p")))
        (if (and (null A) (null B))
            (let ((S (gensym "p"))(F (gensym "p")))
                (if (is-reserved (cdr RE))
                    (list (list S 'epsilon X)
                        (list X 'epsilon Y)
                        (nfa-regexp-comp-switch (second RE) Y W)
                        (list Y 'epsilon W)
                        (nfa-regexp-comp-switch (second RE) W Y)
                        (nfa-regexp-comp-switch (second RE) W Z)
                        (list Z 'epsilon F)
                    )
                    (list (list S 'epsilon X)
                        (list X 'epsilon Y)
                        (list Y (second RE) W)
                        (list Y 'epsilon W)
                        (list W (second RE) Y)
                        (list W (second RE) Z)
                        (list Z 'epsilon F)
                    )
                )
            )
            (if (is-reserved (cdr RE))
                (list (list A 'epsilon X)
                    (list X 'epsilon Y)
                    (nfa-regexp-comp-switch (second RE) Y W)
                    (list Y 'epsilon W)
                    (nfa-regexp-comp-switch (second RE) W Y)
                    (nfa-regexp-comp-switch (second RE) W Z)
                    (list Z 'epsilon B)
                )
                (list (list A 'epsilon X)
                    (list X 'epsilon Y)
                    (list Y (second RE) W)
                    (list Y 'epsilon W)
                    (list W (second RE) Y)
                    (list W (second RE) Z)
                    (list Z 'epsilon B)
                )
            )
        )
    )
)


;; seq

(defun nfa-regexp-comp-seq (RE &optional A B)
    (let ((X (gensym "p")))
        (if (and (null A) (null B))
            (let ((S (gensym "p"))(F (gensym "p")))
                (if (is-reserved (cdr RE))
                    (cons (list S 'epsilon X)
                        (nfa-regexp-comp-seq-ric-res (cdr RE) X F))
                    (cons (list S 'epsilon X)
                        (nfa-regexp-comp-seq-ric (cdr RE) X F))
                )
            )
            (if (is-reserved (cdr RE))
                (cons (list A 'epsilon X)
                    (nfa-regexp-comp-seq-ric-res (cdr RE) X B))
                (cons (list A 'epsilon X)
                    (nfa-regexp-comp-seq-ric (cdr RE) X B))
            )
        )
    )
)


;; Ricorsione per la nfa-regexp-comp di seq

(defun nfa-regexp-comp-seq-ric (RE X F)
    (let ((W (gensym "p")))
        (if (null RE)
            (list(list X 'epsilon F))
            (if (or (null (cdr RE)) (is-reserved (cdr RE)))
                (cons (list X (car RE) W)
                    (nfa-regexp-comp-seq-ric-res (cdr RE) W F))
                (cons (list X (car RE) W)
                    (nfa-regexp-comp-seq-ric (cdr RE) W F))
            )
        )
    )
)

(defun nfa-regexp-comp-seq-ric-res (RE X F)
    (let ((W (gensym "p")))
		(if (null RE)
            (list(list X 'epsilon F))
			(if (or (null (cdr RE)) (is-reserved (cdr RE)))
                (cons (list (nfa-regexp-comp-switch (car RE) X W))
                    (nfa-regexp-comp-seq-ric-res (cdr RE) W F))
                (cons (list (nfa-regexp-comp-switch (car RE) X W))
                    (nfa-regexp-comp-seq-ric (cdr RE) W F))
            )
		)
    )
)


;; or

(defun nfa-regexp-comp-or (RE &optional A B)
    (if (and (null A) (null B))
        (let ((S (gensym "p"))(F (gensym "p")))
            (if (is-reserved (cdr RE))
                (nfa-regexp-comp-or-ric-res (cdr RE) S F)
                (nfa-regexp-comp-or-ric (cdr RE) S F)
            )
        )
        (if (is-reserved (cdr RE))
            (nfa-regexp-comp-or-ric-res (cdr RE) A B)
            (nfa-regexp-comp-or-ric (cdr RE) A B)
        )
    )
)


;; Ricorsione per la nfa-regexp-comp di or

(defun nfa-regexp-comp-or-ric (RE S F)
    (let ((X (gensym "p"))(Y (gensym "p")))
        (if (null RE)
            nil
            (if (or (null (cdr RE)) (is-reserved (cdr RE)))
                (append (list (list S 'epsilon X))
                    (list (list X (car RE) Y))
                    (list (list Y 'epsilon F))
                    (nfa-regexp-comp-or-ric-res (cdr RE) S F)
                )
                (append (list (list S 'epsilon X))
                    (list (list X (car RE) Y))
                    (list (list Y 'epsilon F))
                    (nfa-regexp-comp-or-ric (cdr RE) S F)
                )
            )        
        )
    )
)

(defun nfa-regexp-comp-or-ric-res (RE S F)
    (let ((X (gensym "p"))(Y (gensym "p")))
        (if (null RE)
            nil
            (if (or (null (cdr RE)) (is-reserved (cdr RE)))
                (append (list (list S 'epsilon X))
                    (nfa-regexp-comp-switch (car RE) X Y)
                    (list (list Y 'epsilon F))
                    (nfa-regexp-comp-or-ric-res (cdr RE) S F)
                )
                (append (list (list S 'epsilon X))
                    (nfa-regexp-comp-switch (car RE) X Y)
                    (list (list Y 'epsilon F))
                    (nfa-regexp-comp-or-ric (cdr RE) S F)
                )
            )    
        )        
    )
)


(defun length-l(l)
    (if (null l)
        0
        (+ 1 (length-l (cdr l)))
    )
)


(defun last-l (l)
    (cond ((null l) nil)
        ((null(cdr l)) (car l))
        (T (last-l(cdr l)))
    )
)


;; nfa-test

(defun nfa-test (FA Input)
    (if (listp FA)
        (if (not (is-FA FA))
            (format *error-output* 
                "Error: ~S is not a Finite State Automata." FA)
            (let ((State-moves (state-list FA (car (car FA)))))
                (if (atom Input)
                    nil
                    (nfa-test-ric FA Input State-moves)
                )
            )
        )
        (format *error-output* 
            "Error: ~S is not a Finite State Automata." FA)
    )
)


(defun is-FA (FA)
    (cond ((not (listp (car FA))) nil)
        ((not (equal 3 (length-l (car FA)))) nil)
        ((null (cdr FA)) T)
        (T (is-FA (cdr FA)))
    )
)


(defun state-list (FA state)
    (if (not (null state))
        (if (null FA)
            nil
            (if (equal (car (car FA)) state)
                (append (list (car FA)) (state-list (cdr FA) state))
                (append (state-list (cdr FA) state))
            )
        )
    )
)


;; Ricorsione per nfa-test

(defun nfa-test-ric (FA Input State-moves)
    (if (not (null State-moves))
        (if (null (cdr State-moves))
            (if (not (and (null Input) 
                    (is-final (third (car State-moves)) FA)))
                (if (equal (second (car State-moves)) 'epsilon)
                    (nfa-test-ric FA Input 
                                (state-list FA (third (car State-moves))))
                    (if (equal (second (car State-moves)) (car Input))
                        (nfa-test-ric FA (cdr Input) 
                                (state-list FA (third (car State-moves))))
                        (nfa-test-ric FA Input 
                                (state-list FA (starting-point (cdr FA) FA)))
                    )
                )
                T
            )
            (or (nfa-test-ric FA Input (list (car State-moves)))
                (nfa-test-ric FA Input (cdr State-moves)))
        )
        nil
    )
)


(defun is-final (state FA)
    (if (equal state (third (last-l FA)))
        T
        nil
    )
)


(defun is-initial (state FA)
    (if (equal state (first (car FA)))
        T
        nil 
    )
)


(defun starting-point (FA FA-static)
    (if (null FA)
        nil
        (if (is-initial (first (car FA)) FA-static)
            FA
            (starting-point (cdr FA) FA-static)
        )
    )
)