Componenti del gruppo:

Iris Ermini 845310
Valeria Froio 847272

Commenti e descrizioni del programma svolto:

Predicati principali:
1.	is_regexp/1
2.	nfa_regexp_comp/2
3.	nfa_test/2
4.	nfa_clear/0 e nfa_clear/1
5.	nfa_list/0 e nfa_list/1

Predicati d�appoggio utilizzati:
-	is_compound/1 � vero se RE � un compound con funtore non riservato.
-	is_regularexp/1 viene utilizzato per la ricorsione della is_regexp/1 sugli argomenti di seq e or.
-	is_reserved/1 � vero se RE � un compound con funtore riservato.
-	nfa_regexp_comp/4 costruisce l�automa a seconda dei vari casi.
-	nfa_regexp_comp_ric_seq/4 viene utilizzato per la ricorsione della nfa_regexp_comp/4 sugli argomenti di seq.
-	nfa_regexp_comp_ric_or/4 viene utilizzato per la ricorsione della nfa_regexp_comp/4 sugli argomenti di or.
-	nfa_test_ric/3 viene utilizzato per la ricorsione di nfa_test/2 sulla lista �Input�.

1) is_regexp/1: 
-	utilizza is_compound/1 per trattare diversamente i compound con funtori non riservati rispetto a quelli con funtori riservati.
-	controlla che non si possano avere plus, star, seq e or come atomi.
-	plus e star non vengono trattati ricorsivamente in quanto possono avere solo un argomento.
-	viene controllato che vi siano almeno due argomenti per il caso or.
-	richiama is_regularexp/1 nel caso in cui abbiamo seq oppure or per applicare is_regexp/1 a ogni argomento di seq oppure di or.

2) nfa_regexp_comp/2:
-	si serve di nfa_initial/2 per controllare che non esista gi� un automa con lo stesso �FA_Id� nella base di conoscenza.
-	controlla che l�espressione passata sia regolare con is_regexp/1.
-	crea uno stato iniziale e uno stato finale con nfa_initial/2 e nfa_final/2 e li aggiunge alla base di conoscenza con assert/1.
-	utilizza gensym/2 per generare nomi per gli stati.
-	chiama nfa_regexp_comp/4 per creare un automa a seconda del caso utilizzando gli stati che gli vengono passati.
-	nfa_regexp_comp/4 crea altri stati intermedi e aggiunge alla base di conoscenza la nfa_delta/4 che li collega con un �simbolo� o che contiene una epsilon-mossa.
-	nfa_regexp_comp/4 chiama is_reserved/1 per stabilire se un argomento ha funtore riservato oppure non riservato.
-	per trattare il caso seq, nfa_regexp_comp/4 separa i casi in cui abbiamo solo un argomento con funtore riservato oppure con funtore non riservato e quelli in cui abbiamo pi� argomenti di cui il primo ha un funtore riservato oppure non riservato.
-	in tutti gli altri casi, nfa_regexp_comp/4 considera solo i casi in cui il primo argomento ha un funtore riservato o non riservato.
-	nfa_regexp_comp_ric_seq/4 viene definito per quattro casi diversi: due casi trattano le situazioni in cui abbiamo ancora degli elementi nella lista di argomenti di seq e l�argomento attuale � un compound con funtore riservato oppure � un compound con funtore non riservato, mentre negli altri due abbiamo solo un elemento nella lista e l�argomento attuale � un compound con funtore riservato oppure � un compound con funtore non riservato.
-	nfa_regexp_comp_ric_or/4 � definito in modo analogo a nfa_regexp_comp_ric_seq/4.	

3) nfa_test/2:
-	accetta l�input se lo stato in cui si arriva (con una mossa dettata da nfa_delta/4 partendo da uno stato iniziale) � finale, considerando il caso in cui abbiamo solo un �simbolo� come Input oppure chiama nfa_test_ric/3 tenendo conto che la prima mossa per tutti gli automi � una epsilon-mossa.
-	ci sono due definizioni di nfa_test_ric/3 che controllano le mosse differenziando le epsilon-mosse dalle altre che consumano un �simbolo� dell�Input ricorsivamente e una terza che verifica quando l�Input � vuoto se lo stato ottenuto facendo l�ultima mossa � finale.

4) nfa_clear: 
-	rimuove un automa o tutti gli automi (nfa_initial/2, nfa_delta/4 e nfa_final/2) dalla base di conoscenza chiamando retract/1.
-	utilizzando fail, forza il backtracking che permette di cancellare tutti i fatti che unificano con nfa_delta/4 presenti nel programma.

5) nfa_list:
-	elenca gli stati di un automa o di tutti gli automi (nfa_initial/2, nfa_delta/4 e nfa_final/2), presente/i nella base di conoscenza, chiamando listing/1.
