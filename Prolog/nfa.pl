%%% Iris Ermini 845310
%%% Valeria Froio 847272


:- dynamic nfa_initial/2.

is_compound(RE) :- 
            compound(RE),
            RE =.. [FUN | _],
            FUN \= seq,
            FUN \= or,
            FUN \= plus,
            FUN \= star.

%%% is_regexp/1


%%% Casi base: compound con funtori non riservati e atomic
is_regexp(RE) :-
          atomic(RE),
          RE \= seq,
          RE \= or,
          RE \= plus,
          RE \= star.
                
is_regexp(RE) :- is_compound(RE), !.


%%% plus
is_regexp(plus(RE)) :- is_regexp(RE), !.


%%% star
is_regexp(star(RE)) :- is_regexp(RE), !.


%%% seq
is_regexp(RE) :-
          RE =.. [FUN | REs], 
          FUN = seq, 
          !,  
          is_regularexp(REs).


%%% or
is_regexp(RE) :- 
          RE =.. [FUN | REs], 
          FUN = or, 
          REs = [_ | Q], 
          Q \= [], 
          !, 
          is_regularexp(REs).


is_regexp([RE]) :- is_regexp(RE), !.


is_regularexp([]).
is_regularexp([RE | REs]) :-
                      is_regexp(RE), 
                      !, 
                      is_regularexp(REs).


is_reserved(RE) :- 
            RE =.. [FUN | _], 
            (FUN = seq; FUN = or; FUN = plus; FUN = star), !.


%%% nfa_regexp_comp/2
nfa_regexp_comp(FA_Id, _) :- nfa_initial(FA_Id, _), !.


nfa_regexp_comp(FA_Id, RE) :- 
                      is_regexp(RE),
                      gensym(p, X),
                      asserta(nfa_initial(FA_Id, X)),
                      gensym(p, Y),
                      assertz(nfa_final(FA_Id, Y)),
                      nfa_regexp_comp(FA_Id, RE, X, Y), 
                      !.


%%% atomic
nfa_regexp_comp(FA_Id, RE, S, F) :-
                            atomic(RE),
                            assert(nfa_delta(FA_Id, S, RE, F)), !.


%%% compound con funtore non riservato
nfa_regexp_comp(FA_Id, RE, S, F) :-
                            is_compound(RE),
                            assert(nfa_delta(FA_Id, S, RE, F)), !.


%%% plus
nfa_regexp_comp(FA_Id, RE, S, F) :-
                            RE =.. [FUN | REs],
                            FUN = plus,
                            REs = [R | _],
                            is_reserved(R),
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, X, epsilon, Y)),
                            gensym(p, W),
                            assert(nfa_delta(FA_Id, Y, epsilon, W)),
                            nfa_regexp_comp(FA_Id, R, Y, W),
                            nfa_regexp_comp(FA_Id, R, W, Y),
                            gensym(p, Z),
                            nfa_regexp_comp(FA_Id, R, W, Z),
                            assert(nfa_delta(FA_Id, Z, epsilon, F)), 
                            !.

nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = plus,
                            REs = [R | _],
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, X, epsilon, Y)),
                            gensym(p, W),
                            assert(nfa_delta(FA_Id, Y, epsilon, W)),
                            assert(nfa_delta(FA_Id, Y, R, W)),
                            assert(nfa_delta(FA_Id, W, R, Y)),
                            gensym(p, Z),
                            assert(nfa_delta(FA_Id, W, R, Z)),
                            assert(nfa_delta(FA_Id, Z, epsilon, F)), 
                            !.


%%% star
nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = star,
                            REs = [R | _],
                            is_reserved(R),
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            assert(nfa_delta(FA_Id, X, epsilon, F)),
                            gensym(p, Y),
                            nfa_regexp_comp(FA_Id, R, X, Y),
                            nfa_regexp_comp(FA_Id, R, Y, X),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                            !.

nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = star,
                            REs = [R | _],
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            assert(nfa_delta(FA_Id, X, epsilon, F)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, X, R, Y)),
                            assert(nfa_delta(FA_Id, Y, R, X)),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                            !.
 
 
%%% seq
nfa_regexp_comp(FA_Id, RE, S, F) :-
                            RE =.. [FUN | REs],
                            FUN = seq,
                            REs = [R | REST],
                            REST = [],
                            is_reserved(R),
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)),
                            nfa_regexp_comp(FA_Id, R, X, Y), 
                            !.
 
nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = seq,
                            REs = [R | REST],
                            REST = [],
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)),
                            assert(nfa_delta(FA_Id, X, R, Y)), 
                            !.

nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = seq,
                            REs = [R | REST],
                            is_reserved(R),
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            nfa_regexp_comp(FA_Id, R, X, Y),
                            nfa_regexp_comp_ric_seq(FA_Id, REST, Y, F), 
                            !.

nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = seq,
                            REs = [R | REST],
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, X, R, Y)),
                            nfa_regexp_comp_ric_seq(FA_Id, REST, Y, F), 
                            !.


%%% or
nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = or,
                            REs = [R | REST],
                            is_reserved(R),
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            nfa_regexp_comp(FA_Id, R, X, Y),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)),
                            nfa_regexp_comp_ric_or(FA_Id, REST, S, F), 
                            !.
 
nfa_regexp_comp(FA_Id, RE, S, F) :- 
                            RE =.. [FUN | REs],
                            FUN = or,
                            REs = [R | REST],
                            gensym(p, X),
                            assert(nfa_delta(FA_Id, S, epsilon, X)),
                            gensym(p, Y),
                            assert(nfa_delta(FA_Id, X, R, Y)),
                            assert(nfa_delta(FA_Id, Y, epsilon, F)),
                            nfa_regexp_comp_ric_or(FA_Id, REST, S, F), 
                            !.


%%% Ricorsione per la nfa_regexp_comp della seq
nfa_regexp_comp_ric_seq(FA_Id, RE, S, F) :- 
                                    RE = [R | REST],
                                    REST \= [],
                                    is_reserved(R),
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    nfa_regexp_comp(FA_Id, R, X, Y),
                                    nfa_regexp_comp_ric_seq(FA_Id, REST, Y, F),
                                    !.

nfa_regexp_comp_ric_seq(FA_Id, RE, S, F) :- 
                                    RE = [R | REST],
                                    REST \= [],
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    assert(nfa_delta(FA_Id, X, R, Y)),
                                    nfa_regexp_comp_ric_seq(FA_Id, REST, Y, F),
                                    !.

nfa_regexp_comp_ric_seq(FA_Id, RE, S, F) :- 
                                    RE = [R | _],
                                    is_reserved(R),
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    nfa_regexp_comp(FA_Id, R, X, Y),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                                    !.

nfa_regexp_comp_ric_seq(FA_Id, RE, S, F) :- 
                                    RE = [R | _],
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    assert(nfa_delta(FA_Id, X, R, Y)),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                                    !.


%%% Ricorsione per la nfa_regexp_comp di or
nfa_regexp_comp_ric_or(FA_Id, RE, S, F) :-
                                    RE = [R | REST],
                                    REST \= [],
                                    is_reserved(R),
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    nfa_regexp_comp(FA_Id, R, X, Y),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)),
                                    nfa_regexp_comp_ric_or(FA_Id, REST, S, F),
                                    !.

nfa_regexp_comp_ric_or(FA_Id, RE, S, F) :- 
                                    RE = [R | REST],
                                    REST \= [],
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    assert(nfa_delta(FA_Id, X, R, Y)),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)),
                                    nfa_regexp_comp_ric_or(FA_Id, REST, S, F),
                                    !.

nfa_regexp_comp_ric_or(FA_Id, RE, S, F) :- 
                                    RE = [R | _],
                                    is_reserved(R),
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    nfa_regexp_comp(FA_Id, R, X, Y),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                                    !.

nfa_regexp_comp_ric_or(FA_Id, RE, S, F) :- 
                                    RE = [R | _],
                                    gensym(p, X),
                                    assert(nfa_delta(FA_Id, S, epsilon, X)),
                                    gensym(p, Y),
                                    assert(nfa_delta(FA_Id, X, R, Y)),
                                    assert(nfa_delta(FA_Id, Y, epsilon, F)), 
                                    !.


%%% nfa_test/2


%%% Casi base: compound con funtori non riservati e atomic
nfa_test(FA_Id, Input) :- 
                  Input = [I | Is],
                  Is = [], 
                  nfa_initial(FA_Id, S), 
                  nfa_delta(FA_Id, S, I, N), 
                  nfa_final(FA_Id, N).


%%% Compound con funtori riservati
nfa_test(FA_Id, Input) :- 
                  nfa_initial(FA_Id, S), 
                  nfa_delta(FA_Id, S, epsilon, N), 
                  nfa_test_ric(FA_Id, Input, N).


nfa_test_ric(FA_Id, Input, S) :- 
                          nfa_delta(FA_Id, S, epsilon, N), 
                          nfa_test_ric(FA_Id, Input, N), !.


nfa_test_ric(FA_Id, [I | Is], S) :- 
                            nfa_delta(FA_Id, S, I, N), 
                            nfa_test_ric(FA_Id, Is, N), !. 

nfa_test_ric(FA_Id, [], F):- nfa_final(FA_Id, F), !.


%%% nfa_list/0
nfa_list :- 
      listing(nfa_initial), 
      listing(nfa_delta), 
      listing(nfa_final).


%%% nfa_list/1
nfa_list(FA_Id) :- 
            listing(nfa_initial(FA_Id, _)),
            listing(nfa_delta(FA_Id, _, _, _)),
            listing(nfa_final(FA_Id, _)).


%%% nfa_clear/0
nfa_clear :- 
      retract(nfa_initial(_, _)),
      retract(nfa_delta(_, _, _, _)),
      retract(nfa_final(_, _)),
      fail.


%%% nfa_clear/1
nfa_clear(FA_Id) :- 
              retract(nfa_initial(FA_Id, _)),
              retract(nfa_delta(FA_Id, _, _, _)),
              retract(nfa_final(FA_Id, _)),
              fail.